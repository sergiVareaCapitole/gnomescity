# GnomesCity

I used Angular for create a SPA for solve the problem.

## Installation
When you have downloaded the code, the only thing you will have to do to configure the application is to use the command `npm install`. After that you can run `npm start` for access to the page using the url `http://localhost:4200/`

## Information of pages
 - Home page: In this page, you will can see all the gnome. If you click in some gnome, You will go to the description of the gnome, where if this gnome have friends you can also access the page of the gnome friend by clicking on the name.
 - Search page: In this page, you will can search a gnome by name.

## Commands
 - `npm start`: or a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
 - `npm build`: to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
 - `npm test`: to execute the unit tests via [Karma](https://karma-runner.github.io).


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help`
