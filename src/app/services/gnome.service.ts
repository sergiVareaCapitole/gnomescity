import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';

import { GnomeModel } from '../models/gnome.model';

@Injectable()
export class GnomeService {

  gnomes: GnomeModel[] = [];

  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json',)
      .pipe( map( data => {
        this.gnomes = data['Brastlewark'];
        return data['Brastlewark'];
      }
    ));
  }
}
