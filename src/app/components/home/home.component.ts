import { Component, OnInit } from '@angular/core';

import { GnomeModel } from '../../models/gnome.model';
import { GnomeService } from '../../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  gnomes: GnomeModel[] = [];
  loading: boolean;
  error: boolean;
  mensajeError: string;

  constructor( private readonly gnomeService: GnomeService ) {
    this.getData()

  }

  getData() {
    this.loading = true;
    this.error = false;
    if (this.gnomeService.gnomes.length === 0) {
      this.gnomeService.getData().subscribe( (data: GnomeModel[]) => {
        this.gnomes = data;
        this.loading = false;
      }, ( errorService ) => {
        this.loading = false;
        this.error = true;
        console.log(errorService);
        this.mensajeError = errorService.error.error.message;
      });
    } else {
      this.gnomes = this.gnomeService.gnomes;
    }


  }
}
