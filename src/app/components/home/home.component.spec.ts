import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HomeComponent } from './home.component';
import { GnomeModel } from '../../models/gnome.model';
import { GnomeService } from '../../services';


describe('HomeComponent', () => {
  const mockGnome = new GnomeModel(0, 'test', 'img', 306, 39.0675, 107.75, 'Red', [], []);
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  let gnomeServiceStub: Partial<GnomeService> = {
    getData: jasmine.createSpy('getData').and.returnValue([mockGnome]),
    gnomes: [mockGnome]
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [
        {provide: GnomeService, useValue: gnomeServiceStub}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    gnomeServiceStub = TestBed.get(GnomeService);

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should component gnomes equal to mockGnome', fakeAsync(() => {
    gnomeServiceStub.gnomes = [mockGnome];

    component.getData();
    tick();

    expect(component.gnomes).toEqual([mockGnome]);
    expect(component.error).toBeFalsy();
  }));
});
