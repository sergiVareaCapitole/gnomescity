import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { GnomeService } from '../../services';
import { GnomeModel } from '../../models/gnome.model';

@Component({
  selector: 'app-gnome',
  templateUrl: './gnome.component.html',
  styles: []
})
export class GnomeComponent {

  gnome: GnomeModel;

  constructor(private readonly router: Router, private readonly routerActive: ActivatedRoute,
    private readonly gnomeService: GnomeService) {

    this.routerActive.params.subscribe( params => {
      this.getGnome(params['id']);
    });
  }

  getGnome( gnomeId: string ) {
    this.gnome = this.gnomeService.gnomes.find((gnome: GnomeModel) => gnome.id.toString() === gnomeId);
  }

  showGnome( friend: string ) {
    const gnomeId = this.gnomeService.gnomes.find((gnome: GnomeModel) => gnome.name === friend).id;
    this.router.navigate([ '/gnome', gnomeId  ]);
  }
}
