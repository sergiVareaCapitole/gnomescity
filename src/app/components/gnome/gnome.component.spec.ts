import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { GnomeComponent } from './gnome.component';
import { GnomeModel } from '../../models/gnome.model';
import { GnomeService } from '../../services';


describe('GnomeComponent', () => {
  const mockGnome = new GnomeModel(0, 'test', 'img', 306, 39.0675, 107.75, 'Red', [], []);
  let component: GnomeComponent;
  let fixture: ComponentFixture<GnomeComponent>;

  let gnomeServiceStub: Partial<GnomeService> = {
    getData: jasmine.createSpy('getData').and.returnValue(mockGnome),
    gnomes: [mockGnome]
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GnomeComponent ],
      imports: [RouterTestingModule],
      providers: [
        {provide: GnomeService, useValue: gnomeServiceStub}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GnomeComponent);
    component = fixture.componentInstance;
    gnomeServiceStub = TestBed.get(GnomeService);
    component.gnome = mockGnome;

    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be return the same gnome', () => {
    component.getGnome('0');

    expect(component.gnome).toEqual(mockGnome);
  });
});
