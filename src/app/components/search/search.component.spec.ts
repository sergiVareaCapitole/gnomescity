import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SearchComponent } from './search.component';
import { GnomeModel } from '../../models/gnome.model';
import { GnomeService } from '../../services';


describe('SearchComponent', () => {
  const mockGnome = new GnomeModel(0, 'test', 'img', 306, 39.0675, 107.75, 'Red', [], []);
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  let gnomeServiceStub: Partial<GnomeService> = {
    getData: jasmine.createSpy('getData').and.returnValue([mockGnome]),
    gnomes: [mockGnome]
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchComponent ],
      providers: [
        {provide: GnomeService, useValue: gnomeServiceStub}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    gnomeServiceStub = TestBed.get(GnomeService);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
