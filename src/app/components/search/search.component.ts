import { Component } from '@angular/core';

import { GnomeService } from '../../services';
import { GnomeModel } from '../../models/gnome.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {

  gnomes: GnomeModel[] = [];
  loading: boolean;

  constructor(private gnomeService: GnomeService) { }

  search(word: string) {
    this.loading = true;
    this.gnomes = this.gnomeService.gnomes.filter((gnome: GnomeModel) => gnome.name.toLowerCase().indexOf(word) > -1);
    this.loading = false;
  }
}
