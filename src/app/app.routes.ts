import { Routes } from '@angular/router';
import { GnomeComponent, HomeComponent, SearchComponent } from './components';

export const ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  { path: 'search', component: SearchComponent },
  { path: 'gnome/:id', component: GnomeComponent },
  {path: '', pathMatch: 'full', redirectTo: 'home'}
];
