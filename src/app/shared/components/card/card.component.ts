import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { GnomeModel } from '../../../models/gnome.model';

@Component({
  selector: 'app-cards',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

  @Input() items: GnomeModel[] = [];

  constructor( private router: Router ) { }

  showGnome( gnome: GnomeModel ) {
    this.router.navigate([ '/gnome', gnome.id  ]);
  }
}
