import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// Servicies
import {GnomeService} from './services';

import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';

import { GnomeComponent, HomeComponent, SearchComponent } from './components';
import { CardComponent, LoadingComponent, NavbarComponent } from './shared';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GnomeComponent,
    SearchComponent,
    NavbarComponent,
    LoadingComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot( ROUTES, { useHash: true } )
  ],
  providers: [GnomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
