
export class GnomeModel {
  id: number;
	name:  string;
	thumbnail: string;
  age: number;
  weight: number;
  height: number;
  hair_color: string;
  professions: string[];
  friends: string[];

  constructor(id: number, name:  string, thumbnail: string, age: number, weight: number, height: number,
    hair_color: string, professions: string[], friends: string[]) {
      this.id = id;
    	this.thumbnail = thumbnail;
      this.age = age;
      this.weight = weight;
      this.height = height;
      this.hair_color = hair_color;
      this.professions = professions;
      this.friends = friends;
  }
}
